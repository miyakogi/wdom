WDOM.py
=======

+----------+-------------------------+-----------------------+
| Branch   | Test                    | Coverage              |
+==========+=========================+=======================+
| master   | |Build Status master|   | |codecov.io master|   |
+----------+-------------------------+-----------------------+
| dev      | |Build Status dev|      | |codecov.io dev|      |
+----------+-------------------------+-----------------------+

WORK IN PROGRESS
----------------

WDOM is a browser-based GUI library for python.

.. |Build Status master| image:: https://travis-ci.org/miyakogi/wdom.svg?branch=master
   :target: https://travis-ci.org/miyakogi/wdom
.. |codecov.io master| image:: https://codecov.io/github/miyakogi/wdom/coverage.svg?branch=master
   :target: https://codecov.io/github/miyakogi/wdom?branch=master
.. |Build Status dev| image:: https://travis-ci.org/miyakogi/wdom.svg?branch=dev
   :target: https://travis-ci.org/miyakogi/wdom
.. |codecov.io dev| image:: https://codecov.io/github/miyakogi/wdom/coverage.svg?branch=dev
   :target: https://codecov.io/github/miyakogi/wdom?branch=dev
