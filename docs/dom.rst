DOM Reference
=============

``wdom.dom.Tag`` class provides dom implementation which is syncronized on
python and browser.


.. automodule:: wdom.node

   .. autoclass:: Node
      :members:
      :undoc-members:

   .. autoclass:: Text
      :members:
      :undoc-members:

   .. autoclass:: RawHtml
      :members:
      :undoc-members:

   .. autoclass:: DocumentFragment
      :members:
      :undoc-members:

   .. autoclass:: DocumentType
      :members:
      :undoc-members:

.. automodule:: wdom.document

   .. autoclass:: Document
      :members:
      :undoc-members:

.. automodule:: wdom.element

   .. autoclass:: Attr
      :members:
      :undoc-members:

   .. autoclass:: DOMTokenList
      :members:
      :undoc-members:

   .. autoclass:: NamedNodeMap
      :members:
      :undoc-members:

   .. autoclass:: Element
      :members:
      :undoc-members:

   .. autoclass:: HTMLElement
      :members:
      :undoc-members:

.. automodule:: wdom.event

   .. autoclass:: EventListener
      :members:
      :undoc-members:

   .. autoclass:: EventTarget
      :members:
      :undoc-members:

.. automodule:: wdom.tag

   .. autoclass:: Tag
      :members:
      :undoc-members:

   .. autofunction:: NewTagClass

   .. autoclass:: TagBaseMeta
      :members:
      :undoc-members:
